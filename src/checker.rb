#!/usr/bin/env ruby

require 'colorize'
require 'net/http'
require 'json'
require 'uri'

APP_NAME = File.basename(__FILE__, ".*")
BASE_API_URI = URI("https://repology.org/api/v1/project/")
DEFAULT_FILE_PATH = "packages.txt"
FILE_PATH = ARGV[0] ? ARGV[0] : DEFAULT_FILE_PATH

unless File.exist?(FILE_PATH.strip)
  abort(
"File not found: '#{FILE_PATH}'
Usage: #{$PROGRAM_NAME} file_path (default: packages.txt)"
)
end

def find_newest_version(data)
  data.find { |entry| entry['status'] == 'newest' }&.fetch('version', nil)
end

def filter_results_by_repo(data, repo)
  data.filter { |entry| entry["repo"] == repo }
end

$http = Net::HTTP.new(BASE_API_URI.host, BASE_API_URI.port)
$http.use_ssl = true

def show_package_info(package)
  encoded_package = URI.encode_www_form_component(package)
  url = URI.join(BASE_API_URI, encoded_package)

  begin
    request = Net::HTTP::Get.new(url)
    request['User-Agent'] = APP_NAME
    response = $http.request(request)
    raise "Failed to fetch data: #{response.code} #{response.message}" unless response.is_a?(Net::HTTPSuccess)
    data = JSON.parse(response.body)
  rescue JSON::ParserError => e
    puts "JSON Parsing Error: #{e.message}"
  rescue StandardError => e
    puts "HTTP Request failed: #{e.message}"
  end

  latest_version = find_newest_version(data)
  results = filter_results_by_repo(data, "exherbo")

  results.each do |entry|
      status_output = case entry['status']
          when 'outdated'
          "#{entry['status']} -> #{latest_version}".red
          when 'newest'
          entry['status'].green
          else
          entry['status']
          end
      puts "- #{package} (#{entry['visiblename']} #{entry['version']}): #{status_output}"
    end
end

File.foreach(FILE_PATH) do |line|
  show_package_info(line.strip)
  sleep(1)
end
