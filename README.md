
# Package Info Viewer

## Description

This Ruby script queries the Repology API to fetch and display information about Exherbo software packages. It determines if each package is outdated or the newest version within the Exherbo repositories.

## Requirements

- Ruby
- `colorize` gem
- `net/http` standard library
- `json` standard library
- `uri` standard library

## Installation

1. Install Ruby on your machine
2. Install required gems:

```bash
gem install colorize
```

## Usage

Run the script using the following command:

```bash
./package_info_viewer.rb [file_path]
```

- `file_path`: Optional. Path to a text file containing a list of package names, one per line. Defaults to `packages.txt`

## How It Works

The script reads package names from the specified file, queries the Repology API for each package, and prints the package status. Output includes the package name, visible name, version, and status (colored according to the package's status).
